const { Router } = require("express");
const { check } = require("express-validator");
const {
  createTask,
  getUserTasks,
  deleteTask,
  updateTask,
} = require("../controllers/task");
const { validateData } = require("../middlewares/validateData");
const { validateJWT } = require("../middlewares/validate-jwt");

const router = Router();

router.get("/getByUid/:uidUser", validateJWT, getUserTasks);

router.post(
  "/",
  [
    check("name", "Task name is required").not().isEmpty(),
    validateData,
    validateJWT,
  ],
  createTask
);
router.put("/delete/:uid", validateJWT, deleteTask);
router.put("/update/:uid", validateJWT, updateTask);

module.exports = router;
