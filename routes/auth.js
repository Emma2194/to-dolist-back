/**
 * path : api/login
 */

const { Router } = require("express");
const { check } = require("express-validator");
const { createUser, loginUser, renewToken } = require("../controllers/auth");
const { validateData } = require("../middlewares/validateData");
const { validateJWT } = require("../middlewares/validate-jwt");

const router = Router();

router.post(
  "/new",
  [
    check("username", "Username is required").not().isEmpty(),
    check("password", "Password is required").not().isEmpty(),
    validateData,
  ],
  createUser
);

router.post(
  "/",
  [
    check("username", "Username is required").not().isEmpty(),
    check("password", "Password is required").not().isEmpty(),
    validateData,
  ],
  loginUser
);
// router.get("/verify", validateJWT, verifyToken);
router.get("/renew", validateJWT, renewToken);

module.exports = router;
