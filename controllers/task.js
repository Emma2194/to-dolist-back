const { response } = require("express");
const { generateJWT } = require("../helpers/jwt");
const mongoose = require("mongoose");
const Task = require("../models/task");
const User = require("../models/user");

const createTask = async (req, res = response) => {
  const { name } = req.body;
  try {
    const isExistTaskName = await Task.findOne({ name });
    if (isExistTaskName) {
      return res.status(400).json({
        ok: false,
        msg: "A task with that name already exists",
      });
    }
    const task = new Task(req.body);
    await task.save();

    res.json({
      ok: true,
      task,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      msg: "Contact the administrator",
    });
  }
};

const getUserTasks = async (req, res) => {
  const { uidUser } = req.params;

  try {
    const idObject = mongoose.mongo.ObjectId(uidUser);
    const user = await User.findById(idObject);
    if (!user) {
      return res.status(400).json({
        ok: false,
        msg: "User does not exist",
      });
    }
    const tasks = await Task.find({
      uidUser: user._id,
      status: "ACTIVE",
    }).exec();

    res.json({
      ok: true,
      tasks,
    });
  } catch (error) {
    throw new Error("Contact the administrator");
  }
};

const deleteTask = async (req, res = response) => {
  const { uid } = req.params;
  try {
    const isExistTaskName = await Task.findById(uid);
    if (!isExistTaskName) {
      return res.status(400).json({
        ok: false,
        msg: "Task not found",
      });
    }
    isExistTaskName.status = "INACTIVE";

    await Task.findByIdAndUpdate(
      isExistTaskName._id,
      { $set: isExistTaskName },
      { new: true }
    );

    res.json({
      ok: true,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      msg: "Contact the administrator",
    });
  }
};
const updateTask = async (req, res = response) => {
  const { uid } = req.params;
  const task = req.body;
  try {
    const isExistTaskName = await Task.findById(uid);
    if (!isExistTaskName) {
      return res.status(400).json({
        ok: false,
        msg: "Task not found",
      });
    }

    await Task.findByIdAndUpdate(
      isExistTaskName._id,
      { $set: task },
      { new: true }
    );

    res.json({
      ok: true,
      task,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      msg: "Contact the administrator",
    });
  }
};

module.exports = {
  createTask,
  getUserTasks,
  deleteTask,
  updateTask,
};
