const { response } = require("express");
const bcrypt = require("bcryptjs");
const { validationResult } = require("express-validator");

const { generateJWT } = require("../helpers/jwt");
const User = require("../models/user");

const createUser = async (req, res = response) => {
  const { username, password } = req.body;
  try {
    const isExistUsername = await User.findOne({ username });
    if (isExistUsername) {
      return res.status(400).json({
        ok: false,
        msg: "Username taken",
      });
    }

    const user = new User(req.body);
    const salt = bcrypt.genSaltSync();
    user.password = bcrypt.hashSync(password, salt);
    await user.save();

    const token = await generateJWT(user.id);

    res.json({
      ok: true,
      user,
      token,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      msg: "Contact the administrator",
    });
  }
};

const loginUser = async (req, res) => {
  const { username, password } = req.body;
  try {
    const isExistUser = await User.findOne({ username });

    if (!isExistUser) {
      return res.status(400).json({
        ok: false,
        msg: "Wrong username",
      });
    }
    const validPassword = bcrypt.compareSync(password, isExistUser.password);
    if (!validPassword) {
      return res.status(400).json({
        ok: false,
        msg: "Wrong password",
      });
    }

    const token = await generateJWT(isExistUser.id);

    res.json({
      ok: true,
      user: isExistUser,
      token,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      msg: "Contact the administrator login",
    });
  }
};

const renewToken = async (req, res = response) => {
  const uid = req.uid;

  try {
    const user = await User.findById(uid);

    if (!user) {
      return res.status(400).json({
        ok: false,
        msg: "Wrong data",
      });
    }

    const token = await generateJWT(user.id);

    res.json({
      ok: true,
      user,
      token,
    });
  } catch (error) {
    return res.status(400).json({
      ok: false,
      msg: "Error: Contact the administrator",
    });
  }
};
module.exports = {
  createUser,
  loginUser,
  renewToken,
};
