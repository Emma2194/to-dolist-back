const { response } = require("express");
const jtw = require("jsonwebtoken");

const generateJWT = (uid) => {
  return new Promise((resolve, reject) => {
    const payload = {
      uid,
    };

    jtw.sign(
      payload,
      process.env.JWT_KEY,
      { expiresIn: "12h" },
      (err, token) => {
        if (err) {
          reject("No se pudo generar webtoken");
        } else {
          resolve(token);
        }
      }
    );
  });
};

module.exports = {
  generateJWT,
};
