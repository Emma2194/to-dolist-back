const express = require("express");
const path = require("path");
const CORS = require("cors");

require("dotenv").config();

require("./database/config").dbConnection();

const app = express();

app.use(express.json());
app.use(CORS({ origin: "*" }));

const port = process.env.PORT;
const publicPath = path.resolve(__dirname, "public");
app.use(express.static(publicPath));

//My Routes
app.use("/api/login", require("./routes/auth"));
app.use("/api/task", require("./routes/task"));

app.listen(port, (err) => {
  if (err) throw new Error(err);
});
