const { Schema, model } = require("mongoose");

const TaskSchema = Schema({
  name: {
    type: String,
    required: true,
  },
  done: {
    type: Boolean,
    required: true,
  },
  uidUser: {
    type: Schema.Types.ObjectId,
    ref: "User",
  },
  status: {
    type: String,
    default: "ACTIVE",
  },
});

TaskSchema.method("toJSON", function () {
  const { __v, _id, ...object } = this.toObject();

  object.uid = _id;
  return object;
});

module.exports = model("Task", TaskSchema, "task");
